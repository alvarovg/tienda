#!/usr/bin/env pythoh3

'''
Maneja un diccionario con los artículos de una tienda y
sus precios, y permite luego comprar alguno de sus elementos,
y ver el precio total de la compra.
'''
import sys

#articulos = {"leche": 1, "pan": 2.5}
articulos = {}
def anadir(articulo: str, precio: float):
    """Añade un artículo y un precio al diccionario artículos"""
    articulos[articulo] = precio
#anadir("pan", 1.5)

def mostrar():
    """Muestra en pantalla todos los artículos y sus precios"""
    for i in articulos.keys():
        print(i, end=": ")
        print(f"{articulos[i]} euros")
#mostrar()

def pedir_articulo() -> str:
    """Pide al usuario el artículo a comprar.

    Muestra un mensaje pidiendo el artículo a comprar.
    Cuando el usuario lo escribe, comprueba que es un artículo qie
    está en el diccionario artículos, y termina devolviendo ese
    artículo como string. Si el artículo escrito por el usuario no
    está en artículos, pide otro hasta que escriba uno que sí está."""
    repetir = True
    while repetir:
        print("Artículo: ", end="")
        articulo = str(input())
        if articulo in articulos:
            repetir = False
        else:
            continue
    return articulo
#pedir_articulo()

def pedir_cantidad() -> float:
    """Pide al usuario la cantidad a comprar.

    Muestra un mensaje pidiendo la cantidad a comprar.
    Cuando el usuario lo escribe, comprueba que es un número real
    (float), y termina devolviendo esa cantidad.. Si la cantidad
    escrita no es un float, pide otra hasta que escriba una correcta.
    """
    repetir = True
    while repetir:
        print("Cantidad: ", end="")
        cantidades = input()
        try:
            cantidades = float(cantidades)
            repetir = False
        except ValueError:
            continue

    return cantidades
#pedir_cantidad()

def main():
    """Toma los artículos declarados como argumentos en la línea de comando,
    junto con sus precios, almacénalos en el diccionario artículos mediante
    llamadas a la función añadir. Luego muestra el listado de artículos
    y precios para que el usuario pueda elegir. Termina llamando a las
    funciones pedir_articulo y pedir cantidad, y mostrando en pantalla la
    cantidad comprada, de qué artículo, y cuánto es su precio.
    """
    sys.argv.pop(0)
    if len(sys.argv) != 0:
        try:
            for i in range(0, len(sys.argv), 2):
                anadir(sys.argv[i], float(sys.argv[i + 1]))

            #print(articulos)
            print(f"Lista de artículos en la tienda:") , mostrar()
            print()

            articulof = pedir_articulo()
            cantidadf = pedir_cantidad()
            coste = cantidadf * float(articulos[articulof])
            print()

            print(f"Compra total: {cantidadf} de {articulof}, a pagar {coste}")
        except:
            if len(sys.argv) % 2 != 0:
                print(f"Error en argumentos: Hay al menos un artículo sin precio: {sys.argv[-1]}")
            elif type(sys.argv[i + 1]) is str:
                print(f"Error en argumentos: {sys.argv[-1]} no es un precio correcto")

            raise SystemExit
    else:
        print("Error en argumentos: no se han especificado artículos.")
        raise SystemExit
if __name__ == '__main__':
    main()